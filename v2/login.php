<?php

include("includes/config.php");

include("includes/classes/Account.php");
include("includes/classes/Constants.php");

$account = new Account($con);

include("includes/handlers/login-handler.php");

function getInputValue($name){
  if(isset($_POST[$name]) ){
    echo $_POST[$name];
  }
}

 ?>
 <!-- ============================================= -->
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/07ff1fee41.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="stylesheet" href="assets/css/forms.css">

    <title>Gran Estacion Central | Nos escuchas aqui y ahora</title>
</head>
<body>

    <!-- Image and text -->
    <nav class="navbar navbar-dark bg-dark p-4">
        <a class="navbar-brand" href="index.php">
          <img src="./assets/logos/gec.png" width="100" height="100" class="d-inline-block mx-2" alt="">
          #AquíYAhora
        </a>
        <span class="navbar-text">
            <p>¡ Siguenos !</p>
            <a href="#"> <i class="fab fa-facebook fa-2x"></i></a>
            <a href="#"> <i class="fab fa-instagram fa-2x"></i> </a>
            <a href="#"> <i class="fab fa-twitter fa-2x"></i> </a>
        </span>
    </nav>

          <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Gran Estación Central</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="londonastrologia.html">London Astrología</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="heterotopia.html">Heterotopía</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="enlaruta.html">En La Ruta</a>
        </li>

      </ul>
    </div>
  </nav>

    <div class="container-fluid">
        <div class="jumbotron">
            <p class="lead">Es necesario que ingrese para poder agregar un podcast nuevo.</p>

        </div>
        <form action="login.php" method="POST">

          <div class="form-group">
            <p>
              <?php
    							echo $account->getError(Constants::$loginFallido);
    						?>
            </p>
            <label for="usuario">Usuario</label>
            <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Ejemplo: pepitoGrillo" required>

          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
          </div>

          <button type="submit" name="loginButton" class="btn btn-primary">Entrar</button>
        </form>

    </div>

    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0"> <a href="http://diegomartinez.website">Diego Martinez</a></span>
    </nav>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</html>
