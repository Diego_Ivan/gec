<?php
  include("includes/partials/header.php");
 ?>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Heterotopía</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="londonastrologia.php">London Astrología</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="enlaruta.php">En La Ruta</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="granestacioncentral.php">Gran Estación Central</a>
      </li>

    </ul>
  </div>
</nav>


        <div class="jumbotron">
            <h1 class="display-4">Heterotopía</h1>
            <p class="lead">Ruta por la cual, las actividades artísticas y culturales de la población LGBTQI de México se hacen presentes, a través de sus protagonistas. Dando a conocer el movimiento queer mexicano, reforzamos el concepto de otredad y reflexionamos sobre los procesos de normalización y de exclusión internos.</p>
            <p class="lead"> “Los otros todos que nosotros somos” -Octavio Paz</p>
        </div>

        <?php

          $principalQuery = mysqli_query($con, "SELECT * FROM podcasts WHERE programa='Heterotopia' && podcast_id=(SELECT max(podcast_id) FROM podcasts)");

          while($row = mysqli_fetch_array($principalQuery) ){

            echo "<div class='container-fluid'>
              <div class='jumbotron' id='jumbo-play'>
                  <h1 class='display-4'>" . $row['episodio'] . "</h1>
                  <p>" . $row['fecha'] . "</p>
                  <p class='lead'>" .$row['descripcion'] . "</p>

                  <audio src=' ". $row['ubicacion'] ." ' controls>

              </div>
            </div>";
          }

         ?>

        <div class="container-fluid">

                  <div class="container-fluid">
                    <h2 class="card-title">Más Podcasts</h2>


                    <?php
                      $recentsQuery = mysqli_query($con, "SELECT * FROM podcasts WHERE programa='Heterotopia' ORDER BY podcast_id DESC LIMIT 1,5");

                        while($row = mysqli_fetch_array($recentsQuery) ){
                             echo "
                             <div class='container-fluid'>
                               <div class='jumbotron jumbotron-fluid' id='player-min'>
                                 <div class='container'>
                                   <h1 class='display-5'>" . $row['episodio'] ."</h1>
                                   <p>". $row['fecha'] ."</p>
                                   <p>". $row['descripcion'] ."</p>
                                   <audio src=' ". $row['ubicacion'] . " ' controls>
                                 </div>
                               </div>
                             </div>
                             ";
                             }

                      ?>

                </div>



                <div class="navbar navbar-expand-lg navbar-light">

                  <div class="card-body text-center">
                    <h5 class="card-title">Escuchanos en:</h5>
                    <p class="card-text">
                        <img src="./assets/logos/spotify.jpg" width="80" height="80" class="d-inline-block mx-2" alt="">
                        <a href="https://itunes.apple.com/mx/podcast/en-la-ruta/id1028743976?mt=2"> <img src="./assets/logos/iTunes.png" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                        <a href="https://mx.ivoox.com/es/podcast-en-la-ruta_fg_f1160026_filtro_1.xml"> <img src="./assets/logos/ivoox.png" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                        <a href="https://www.podomatic.com/podcasts/mafr197550519"> <img src="./assets/logos/podomatic.jpg" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                    </p>

                  </div>
                </div>
        </div>
<?php
  include("includes/partials/footer.php");
?>
