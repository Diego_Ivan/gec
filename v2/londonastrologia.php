<?php

  include("includes/config.php");

 ?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/07ff1fee41.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="stylesheet" href="assets/css/land.css">


    <title>Gran Estacion Central | Nos escuchas aqui y ahora</title>
</head>

<body>
  <nav class="navbar navbar-dark bg-dark p-4">
      <a class="navbar-brand" href="index.php">
        <img src="./assets/logos/gec.png" width="100" height="100" class="d-inline-block mx-2" alt="">
        #AquíYAhora
      </a>
      <span class="navbar-text">
          <p>¡ Siguenos !</p>
          <a href="https://www.facebook.com/elmitodelondon/"> <i class="fab fa-facebook fa-2x"></i></a>
          <a href="https://www.instagram.com/elmitodelondon/"> <i class="fab fa-instagram fa-2x"></i> </a>
          <a href="https://twitter.com/elmitodelondon"> <i class="fab fa-twitter fa-2x"></i> </a>
      </span>
  </nav>

 <nav class="navbar navbar-expand-lg navbar-light bg-light">
 <a class="navbar-brand" href="#">London Astrología</a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
   <span class="navbar-toggler-icon"></span>
 </button>
 <div class="collapse navbar-collapse" id="navbarNav">
   <ul class="navbar-nav">
     <li class="nav-item active">
       <a class="nav-link" href="granestacioncentral.php">Gran Estación Central</a>
     </li>
     <li class="nav-item active">
       <a class="nav-link" href="heterotopia.php">Heterotopía</a>
     </li>
     <li class="nav-item active">
       <a class="nav-link" href="enlaruta.php">En La Ruta</a>
     </li>

   </ul>
 </div>
</nav>


  <div class="jumbotron">
      <h1 class="display-4">London Astrología</h1>
      <p class="lead">Sube la mirada al cielo, sin perder el rumbo. Descubre como los astros siempre están a tu favor, siempre y cuando conozcas sus cualidades y efectos que tienen sobre nosotros.</p>
      <p class="lead">"La vida sólo puede ser comprendida mirando hacia atrás, pero ha de ser vivida mirando hacia adelante." -Sören Aabye Kierkegaard</p>
  </div>

  <?php

    $principalQuery = mysqli_query($con, "SELECT * FROM podcasts WHERE programa='London Astrologia' && podcast_id=(SELECT max(podcast_id) FROM podcasts)");

    while($row = mysqli_fetch_array($principalQuery) ){

      echo "<div class='container-fluid'>
        <div class='jumbotron' id='jumbo-play'>
            <h1 class='display-4'>" . $row['episodio'] . "</h1>
            <p>" . $row['fecha'] . "</p>
            <p class='lead'>" .$row['descripcion'] . "</p>

            <audio src=' ". $row['ubicacion'] ." ' controls>

        </div>
      </div>";
    }

   ?>

  <div class="container-fluid">

            <div class="container-fluid">
              <h2 class="card-title">Más Podcasts</h2>


              <?php
                $recentsQuery = mysqli_query($con, "SELECT * FROM podcasts WHERE programa='London Astrologia' ORDER BY podcast_id DESC LIMIT 1,5");

                  while($row = mysqli_fetch_array($recentsQuery) ){
                       echo "
                       <div class='container-fluid'>
                         <div class='jumbotron jumbotron-fluid' id='player-min'>
                           <div class='container'>
                             <h1 class='display-5'>" . $row['episodio'] ."</h1>
                             <p>". $row['fecha'] ."</p>
                             <p>". $row['descripcion'] ."</p>
                             <audio src=' ". $row['ubicacion'] . " ' controls>
                           </div>
                         </div>
                       </div>
                       ";
                       }

                ?>


          </div>



          <div class="navbar navbar-expand-lg navbar-light">

            <div class="card-body text-center">
              <h5 class="card-title">Escuchanos en:</h5>
              <p class="card-text">
                  <a href="https://open.spotify.com/show/6WOmaWnw9MwFjfp3EXXtJK?si=sLLyVvQaSsmTJXw8AgMWzA"> <img src="./assets/logos/spotify.jpg" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                  <a href="https://itunes.apple.com/mx/podcast/london-astrolog%C3%ADa/id1099358273?mt=2"> <img src="./assets/logos/iTunes.png" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                  <a href="https://mx.ivoox.com/es/podcast-london-astrologia_fg_f1279556_filtro_1.xml"> <img src="./assets/logos/ivoox.png" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                  <a href="https://www.podomatic.com/podcasts/londonastrologia"> <img src="./assets/logos/podomatic.jpg" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
              </p>

            </div>
          </div>
  </div>
<?php
 include("includes/partials/footer.php");
?>
