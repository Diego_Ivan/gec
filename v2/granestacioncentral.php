<?php
  include("includes/partials/header.php");
 ?>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Gran Estación Central</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="londonastrologia.php">London Astrología</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="heterotopia.php">Heterotopía</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="enlaruta.php">En La Ruta</a>
      </li>

    </ul>
  </div>
</nav>

        <div class="jumbotron">
            <h1 class="display-4">Gran Estación Central</h1>
            <p class="lead">Así, como en una estación de autobuses, aviones o trenes, Gran Estación Central es el punto de encuentro idóneo para el cruce de ideas, proyectos y propuestas creativas</p>
            <p class="lead">"Lo único constante es el cambio" -Heráclito</p>
        </div>

        <?php

          $principalQuery = mysqli_query($con, "SELECT * FROM podcasts WHERE programa='Gran Estacion Central' && podcast_id=(SELECT max(podcast_id) FROM podcasts)");

          while($row = mysqli_fetch_array($principalQuery) ){

            echo "<div class='container-fluid'>
              <div class='jumbotron' id='jumbo-play'>
                  <h1 class='display-4'>" . $row['episodio'] . "</h1>
                  <p>" . $row['fecha'] . "</p>
                  <p class='lead'>" .$row['descripcion'] . "</p>

                  <audio src=' ". $row['ubicacion'] ." ' controls>

              </div>
            </div>";
          }

         ?>

        <div class="container-fluid">

                  <div class="container-fluid">
                    <h2 class="card-title">Más Podcasts</h2>


                    <?php
		                  $recentsQuery = mysqli_query($con, "SELECT * FROM podcasts WHERE programa='Gran Estacion Central' ORDER BY podcast_id DESC LIMIT 1,5");

		                    while($row = mysqli_fetch_array($recentsQuery) ){
			                       echo "
                             <div class='container-fluid'>
                               <div class='jumbotron jumbotron-fluid' id='player-min'>
                                 <div class='container'>
                                   <h1 class='display-5'>" . $row['episodio'] ."</h1>
                                   <p>". $row['fecha'] ."</p>
                                   <p>". $row['descripcion'] ."</p>
                                   <audio src=' ". $row['ubicacion'] . " ' controls>
                                 </div>
                               </div>
                             </div>
                             ";
                             }

	                    ?>


                </div>



                <div class="navbar navbar-expand-lg navbar-light">

                  <div class="card-body text-center">
                    <h5 class="card-title">Escuchanos en:</h5>
                    <p class="card-text">
                        <a href="https://open.spotify.com/show/5lJeA9HeslHt6WCE32AQ3e?si=lJWi0FopSxinnBbCRWuWiw"> <img src="./assets/logos/spotify.jpg" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                        <a href="https://itunes.apple.com/mx/podcast/gran-estaci%C3%B3n-central/id1104669958?mt=2"> <img src="./assets/logos/iTunes.png" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                        <a href="https://open.spotify.com/show/5lJeA9HeslHt6WCE32AQ3e?si=lJWi0FopSxinnBbCRWuWiw"> <img src="./assets/logos/ivoox.png" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                        <a href="https://www.podomatic.com/podcasts/granestacioncentral"> <img src="./assets/logos/podomatic.jpg" width="80" height="80" class="d-inline-block mx-2" alt=""> </a>
                    </p>

                  </div>
                </div>
        </div>
<?php
  include("includes/partials/footer.php");
?>
