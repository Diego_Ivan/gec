<?php

  include("includes/config.php");

 ?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/07ff1fee41.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="stylesheet" href="assets/css/land.css">


    <title>Gran Estacion Central | Nos escuchas aqui y ahora</title>
</head>

<body>
  <nav class="navbar navbar-dark bg-dark p-4">
      <a class="navbar-brand" href="index.php">
        <img src="./assets/logos/gec.png" width="100" height="100" class="d-inline-block mx-2" alt="">
        #AquíYAhora
      </a>
      <span class="navbar-text">
          <p>¡ Siguenos !</p>
          <a href="https://www.facebook.com/granestacioncentral/"> <i class="fab fa-facebook fa-2x"></i></a>
          <a href="https://www.instagram.com/granestacionc/"> <i class="fab fa-instagram fa-2x"></i> </a>
          <a href="https://twitter.com/granestacionc"> <i class="fab fa-twitter fa-2x"></i> </a>
      </span>
  </nav>
