<?php

  class Constants{

    public static $loginFallido = "Usuario o Password incorrectos";
    public static $programaCharacters = "Deja de intentar eso";
    public static $pathCharacters = "La direccion debe tener entre 10 y 200 caracteres";
    public static $podcastTaken = "Ya se ha registrado esta dirección antes, el podcast ya está en el sistema";
    public static $episodeCharacters = "El nombre del episodio debe contener un máximo de 200 caracteres";
    public static $episodeTaken = "El nombre del episodio ya se ha registrado antes";
    public static $descCharacters = "La descripcion del episodio debe contener entre 20 y 300 caracteres";
    public static $descTaken = "Ya se ha ingresado esta descripcion antes";

  }

 ?>
