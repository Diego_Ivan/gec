<?php

  class Podcast{

    private $con;
    private $errorArray;

    public function __construct($con) {
			$this->con = $con;
			$this->errorArray = array();
		}

    public function register($pg, $ph, $ep, $ds){
      $this->validatePrograma($pg);
      $this->validatePath($ph);
      $this->validateEpisode($ep);
      $this->validateDesc($ds);

      if(empty($this->errorArray) ){
        return $this->insertPodcastDetails($pg, $ph, $ep, $ds);
      } else {
        return false;
      }

    }

    public function getError($error){
      if(!in_array($error, $this->errorArray)){
        $error = "";
      }
      return "<span class='mensajeError'>$error</span>";
    }

    private function insertPodcastDetails($pg, $ph, $ep, $ds){
      $date = date("Y-m-d");
      echo "INSERT INTO podcasts (programa, ubicacion, episodio, descripcion, fecha) VALUES ('$pg', '$ph', '$ep', '$ds', '$date')";
      $result = mysqli_query($this->con, "INSERT INTO podcasts (programa, ubicacion, episodio, descripcion, fecha) VALUES ('$pg', '$ph', '$ep', '$ds', '$date')");

      return $result;
    }

    private function validatePrograma($pg){
      if(strlen($pg) > 30 || strlen($pg) < 5 ){
        array_push($this->errorArray, Constants::$programaCharacters);
        return;
      }

    }

    private function validatePath($ph){
      if(strlen($ph) > 200 || strlen($ph) < 10 ){
        array_push($this->errorArray, Constants::$pathCharacters);
        return;
      }

      $checkPathQuery = mysqli_query($this->con, "SELECT ubicacion FROM podcasts WHERE ubicacion='$ph'");
      if (mysqli_num_rows($checkPathQuery) !=0 ){
        array_push($this->errorArray, Constants::$podcastTaken);
        return;
      }

    }

    private function validateEpisode($ep){
      if(strlen($ep) > 200 || strlen($ep) < 5 ){
        array_push($this->errorArray, Constants::$episodeCharacters);
        return;
      }

      $checkEpisodeQuery = mysqli_query($this->con, "SELECT episodio FROM podcasts WHERE episodio='$ep'");
      if (mysqli_num_rows($checkEpisodeQuery) !=0 ){
        array_push($this->errorArray, Constants::$episodeTaken);
        return;
      }


    }

    private function validateDesc($ds){
      if(strlen($ds) > 300 || strlen($ds) < 20){
        array_push($this->errorArray, Constants::$descCharacters);
        return;
      }
      $checkDescQuery = mysqli_query($this->con, "SELECT descripcion FROM podcasts WHERE descripcion='$ds'");
      if (mysqli_num_rows($checkPathQuery) !=0 ){
        array_push($this->errorArray, Constants::$descTaken);
        return;
      }

    }


  }

 ?>
