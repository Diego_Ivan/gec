<?php

  include("includes/config.php");

  include("includes/classes/Podcast.php");
  include("includes/classes/Constants.php");

  $podcast = new Podcast($con);

  include("includes/handlers/podcast-handler.php");

  function getInputValue($name){
    if(isset($_POST[$name]) ){
      echo $_POST[$name];
    }
  }

  if(isset($_SESSION['userLoggedIn']) ){
		$userLoggedIn = $_SESSION['userLoggedIn'];
	}else{
		header("Location: login.php");
	}

 ?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/07ff1fee41.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="stylesheet" href="assets/css/forms.css">

    <title>Gran Estacion Central | Nos escuchas aqui y ahora</title>
</head>
<body>

    <!-- Image and text -->
    <nav class="navbar navbar-dark bg-dark p-4">
        <a class="navbar-brand" href="index.php">
          <img src="./assets/logos/gec.png" width="100" height="100" class="d-inline-block mx-2" alt="">
          #AquíYAhora
        </a>
        <span class="navbar-text">
            <p>¡ Siguenos !</p>
            <i class="fab fa-facebook fa-2x"></i>
            <i class="fab fa-instagram fa-2x"></i>
            <i class="fab fa-twitter fa-2x"></i>
        </span>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="font-family:Cabin">
        <a class="navbar-brand" href="granestacioncentral.php">Gran Estación Central</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="londonastrologia.php">London Astrología</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="heterotopia.php">Heterotopía</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="enlaruta.php">En La Ruta</a>
                </li>

            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <form action="nuevoPodcast.php" method="post">
            <div class="jumbotron">
            <p class="lead">Ingrese la informacion de su podcast.</p>

            </div>

            <div class="form-group">
              <?php
							echo $podcast->getError(Constants::$programaCharacters);
						  ?>
              <label for="programa">Seleccione el programa del podcast</label>
              <select class="form-control" id="programa" name="programa">
                <option>Gran Estacion Central</option>
                <option>London Astrologia</option>
                <option>Heterotopia</option>
                <option>En La Ruta</option>
            </select>
            </div>
            <div class="form-group">
              <?php
              echo $podcast->getError(Constants::$pathCharacters);
              ?>
              <?php
              echo $podcast->getError(Constants::$podcastTaken);
              ?>
                <label for="fileName">Path del archivo</label>
                <input type="text" class="form-control" id="fileName" name="fileName" aria-describedby="emailHelp" placeholder="Ejemplo: assets/images/artwork/image2.jpg" required>
                <small id="emailHelp" class="form-text text-muted">El archivo debe estar ya en el servidor</small>
            </div>

            <div class="form-group">
              <?php
              echo $podcast->getError(Constants::$episodeCharacters);
              ?>
              <?php
              echo $podcast->getError(Constants::$episodeTaken);
              ?>

                <label for="episodeName">Nombre del episodio</label>
                <input type="text" class="form-control" id="episodeName" name="episodeName" aria-describedby="emailHelp" placeholder="Ejemplo:  Buenos Aires 2019" required>
                <small id="emailHelp" class="form-text text-muted">Ingrese el nombre del episodio que se va a mostrar en la página de su podcast</small>
            </div>

            <div class="form-group">
              <?php
              echo $podcast->getError(Constants::$descCharacters);
              ?>
              <?php
              echo $podcast->getError(Constants::$descTaken);
              ?>
                <label for="descripcion">Texto descripcion del episodio</label>
                <textarea class="form-control" id="descripcion" name="descripcion" rows="3" placeholder="Ingrese la descripcion que aparecerá sobre este episodio" required></textarea>
            </div>

          <button type="submit" name="registerButton" class="btn btn-primary">Todo Listo</button>
        </form>

    </div>


</html>
