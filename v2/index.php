<?php
  include("includes/partials/header.php");
 ?>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Gran Estación Central</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="londonastrologia.php">London Astrología</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="heterotopia.php">Heterotopía</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="enlaruta.php">En La Ruta</a>
      </li>

    </ul>
  </div>
</nav>
    <div class="container-fluid text-center">

        <div class="section my-5">
            <div class="embed-responsive embed-responsive-16by9">
                <video playsinline autoplay muted loop src="./assets/video/primary.mp4"></video>
            </div>
          </div>

          <div class="section">

            <div class="card-deck">

              <div class="card border-0">
                <img src="./assets/logos/gec.png" class="card-img-top">

                <div class="card-body">
                  <h5 class="card-title">Gran Estación Central</h5>
                  <p class="card-text">Así, como en una estación de autobuses...</p>
                  <a href="granestacioncentral.php" class="btn btn-primary btn-block">¡ Escúchanos !</a>
                </div>
              </div>

              <div class="card border-0">
                  <img src="./assets/logos/london.png" class="card-img-top">
                  <div class="card-body">
                    <h5 class="card-title">London Astrología</h5>
                    <p class="card-text">Sube la mirada hacia el cielo, sin perder el rumbo. Descubre como...  </p>
                    <a href="londonastrologia.php" class="btn btn-primary btn-block">¡ Escúchanos !</a>
                  </div>
                </div>

                <div class="card border-0">
                    <img src="./assets/logos/heterotopia.png" class="card-img-top">
                    <div class="card-body">
                      <h5 class="card-title">Heterotopía</h5>
                      <p class="card-text">Ruta por la cual, las actividades artísticas y culturales de la...</p>
                      <a href="heterotopia.php" class="btn btn-primary btn-block">¡ Escúchanos !</a>
                    </div>
                  </div>

                  <div class="card border-0">
                      <img src="./assets/logos/enlaruta.JPG" class="card-img-top">
                      <div class="card-body">
                        <h5 class="card-title">En La Ruta</h5>
                        <p class="card-text">Donde se materializan y forman parte de tu cotidiano. No te limites para...</p>
                        <a href="enlaruta.php" class="btn btn-primary btn-block">¡ Escúhanos !</a>
                      </div>
                    </div>


            </div>
          </div>


        </div>

    <?php
    include("includes/partials/footer.php");
     ?>
